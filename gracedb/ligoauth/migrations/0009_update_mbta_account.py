# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-06-13 16:50
#
# This migration updates certificates for the MBTA account

from __future__ import unicode_literals
from django.db import migrations

ACCOUNT = {
    'username': 'MbtaAlert',
    'remove_certs': [
        '/C=IT/O=INFN/OU=Service/L=EGO/CN=MbtaAlert/olnode33.virgo.infn.it',
        '/C=IT/O=INFN/OU=Service/L=EGO/CN=MbtaAlert/olnode04.virgo.infn.it',
    ],
    'new_certs': [
        '/DC=org/DC=ligo/O=LIGO/OU=Services/CN=MbtaAlert/lscgw.virgo.infn.it',
    ],
}


def update_certs(apps, schema_editor):
    LocalUser = apps.get_model('ligoauth', 'LocalUser')
    X509Cert = apps.get_model('ligoauth', 'X509Cert')

    # Get user
    user = LocalUser.objects.get(username=ACCOUNT['username'])

    # Delete old certificates
    for subject in ACCOUNT['remove_certs']:
        X509Cert.objects.get(subject=subject).delete()

    # Create new certificates and associate with user
    for subject in ACCOUNT['new_certs']:
        new_cert = X509Cert.objects.create(subject=subject)
        user.x509cert_set.add(new_cert)


def revert_certs(apps, schema_editor):
    LocalUser = apps.get_model('ligoauth', 'LocalUser')
    X509Cert = apps.get_model('ligoauth', 'X509Cert')

    # Get user
    user = LocalUser.objects.get(username=ACCOUNT['username'])

    # Create old certificates and associate with user
    for subject in ACCOUNT['remove_certs']:
        cert = X509Cert.objects.create(subject=subject)
        user.x509cert_set.add(cert)

    # Delete new certs
    for subject in ACCOUNT['new_certs']:
        X509Cert.objects.get(subject=subject).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0008_add_cwb_certs'),
    ]

    operations = [
        migrations.RunPython(update_certs, revert_certs),
    ]
