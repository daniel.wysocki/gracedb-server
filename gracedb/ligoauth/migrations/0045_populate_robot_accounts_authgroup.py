# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-06-18 16:42
from __future__ import unicode_literals

from django.db import migrations

ROBOT_GROUP_NAME = 'robot_accounts'


# Add current RobotUsers to robot_accounts AuthGroup
def add_users(apps, schema_editor):
    AuthGroup = apps.get_model('ligoauth', 'AuthGroup')
    RobotUser = apps.get_model('ligoauth', 'RobotUser')

    group = AuthGroup.objects.get(name=ROBOT_GROUP_NAME)
    for robot in RobotUser.objects.all():
        group.user_set.add(robot.user_ptr)


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0044_create_robot_accounts_authgroup'),
    ]

    operations = [
        migrations.RunPython(add_users, migrations.RunPython.noop),
    ]
