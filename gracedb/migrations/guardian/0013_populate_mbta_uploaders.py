# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-01 16:19
from __future__ import unicode_literals

from django.db import migrations

# populates "MBTA" uploaders from "MBTAOnline"

def add_permissions(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    Permission = apps.get_model('auth', 'Permission')
    UserObjectPermission = apps.get_model('guardian', 'UserObjectPermission')
    Pipeline = apps.get_model('events', 'Pipeline')
    ContentType = apps.get_model('contenttypes', 'ContentType')

    perm = Permission.objects.get(codename='populate_pipeline')
    ctype = ContentType.objects.get_for_model(Pipeline)

    mbtaonline = Pipeline.objects.get(name='MBTAOnline')
    mbta = Pipeline.objects.get(name='MBTA')

    perms = UserObjectPermission.objects.filter(object_pk=mbtaonline.pk,
                permission=perm,
                content_type=ctype)
    
    mbta_users = [p.user for p in perms]

    for u in mbta_users:

        uop, uop_created = UserObjectPermission.objects.get_or_create(
            user=u, permission=perm, content_type=ctype,
            object_pk=mbta.id)

def remove_permissions(apps, schema_editor):
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('guardian', '0012_add_swift_subthrehold_permission'),
        ('events', '0051_add_MBTA_pipeline'),
    ]

    operations = [
        migrations.RunPython(add_permissions, remove_permissions),
    ]
