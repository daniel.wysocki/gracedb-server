# Generated by Django 3.2.9 on 2021-11-17 23:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0026_auto_20200303_0151'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='permission',
            options={'ordering': ['content_type__app_label', 'content_type__model', 'codename'], 'verbose_name': 'permission', 'verbose_name_plural': 'permissions'},
        ),
        migrations.AlterField(
            model_name='user',
            name='first_name',
            field=models.CharField(blank=True, max_length=150, verbose_name='first name'),
        ),
    ]
